FROM node:8.16-alpine
WORKDIR /app
COPY package.json /app
RUN npm install
COPY ./src /app/src
WORKDIR /app/src
CMD node app.js
EXPOSE 80