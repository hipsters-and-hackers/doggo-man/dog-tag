# Frontend (User Facing) Service for Doggo Man Web Application

## For Development
1. Create docker image from docker file  
`docker build -t dog-tag .`
2. Create container based on the docker image  
`docker run -p 80:80 --name dog-tag dog-tag`  
`docker run -v "$(pwd)"/src:/app/src -p 80:80 -d --name dog-tag dog-tag`  
^ bind local app files to container for easier development  
3. Access localhost:8081